import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagesGallaryComponent } from './images-gallary.component';

describe('ImagesGallaryComponent', () => {
  let component: ImagesGallaryComponent;
  let fixture: ComponentFixture<ImagesGallaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImagesGallaryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImagesGallaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
