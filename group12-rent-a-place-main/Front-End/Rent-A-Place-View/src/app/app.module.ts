import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { HomeComponent } from './pages/home/home.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProductComponent } from './pages/product/product.component';
import { AdminDashboardComponent } from './pages/admin/admin-dashboard/admin-dashboard.component';
import { AdminProductComponent } from './pages/admin/admin-product/admin-product.component';
import { MyOrderComponent } from './pages/my-order/my-order.component';
import { CustomerListComponent } from './pages/admin/customer-list/customer-list.component';
import { WishlistComponent } from './pages/wishlist/wishlist.component';
import { UserDetailsComponent } from './pages/user-details/user-details.component';
import { AuthService } from './services/auth.service';
import { ApiService } from './services/api.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertService } from './services/alert/alert.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialExampleModule } from 'src/material.module';
import { SessionService } from './services/storage/session.service';
import { AuthComponent } from './pages/auth/auth.component';
import { ProductService } from './services/product/product.service';
import { CustomerService } from './services/customer/customer.service';
import { LayoutComponent } from './pages/admin/layout/layout.component';
import { FeedbackComponent } from './pages/admin/feedback/feedback.component';
import { OrderListComponent } from './pages/admin/order-list/order-list.component';
import { FeedbackService } from './services/feedback/feedback.service';
import { UserFeedbackComponent } from './pages/user-feedback/user-feedback.component';
import { BookingComponent } from './pages/booking/booking.component';
import { MessageReplyComponent } from './components/message-reply/message-reply.component';
import { UpdatePropertyComponent } from './components/update-property/update-property.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { BoookPropertyComponent } from './components/boook-property/boook-property.component';
import { ImagesGallaryComponent } from './components/images-gallary/images-gallary.component';
import { AddImagesComponent } from './pages/add-images/add-images.component';
@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    SignInComponent,
    HomeComponent,
    DashboardComponent,
    ProductComponent,
    AdminDashboardComponent,
    AdminProductComponent,
    MyOrderComponent,
    CustomerListComponent,
    WishlistComponent,
    UserDetailsComponent,
    AuthComponent,
    LayoutComponent,
    OrderListComponent,
    FeedbackComponent,
    UserFeedbackComponent,
    BookingComponent,
    MessageReplyComponent,
    UpdatePropertyComponent,
    BoookPropertyComponent,
    ImagesGallaryComponent,
    AddImagesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialExampleModule,
    ReactiveFormsModule,

  ],
  providers: [
    AuthService,
    ApiService,
    AlertService,
    SessionService,
    ProductService,
    CustomerService,
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
