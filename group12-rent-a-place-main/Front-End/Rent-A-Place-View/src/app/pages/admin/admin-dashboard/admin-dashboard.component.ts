import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { UpdatePropertyComponent } from 'src/app/components/update-property/update-property.component';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api.service';
import { ProductDataType, ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  products: ProductDataType[] = [];
  allRecordsCount = 0;
  pageSize = 8;
  pageEvent: any;
  properties: any[] = [];


  constructor(
    private http: ApiService,
    private alert: AlertService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getProducts(0);
  }
  onPageChange(event: PageEvent) {
    this.getProducts(event.pageIndex);
  }

  getProducts(pageNumber: number) {
    this.http.get(`/api/properties?pageNumber=${pageNumber}&recordsPerPage=${this.pageSize}`)
      .subscribe((res: any) => {
        this.properties = res?.content;
        this.allRecordsCount = res?.totalElements;
      })
  }


  onRUpdate(data: any) {
    const dialogRef = this.dialog.open(UpdatePropertyComponent, {
      width: '500px',
      data: { property: data },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.getProducts(0);
    });
  }


  rateAr(range: number) {
    return Array(range).fill(1);
  }

  onDelete(propertyId: number) {
    this.http.delete(`/api/properties/delete/${propertyId}`).subscribe(res => {
      this.getProducts(0);
    })
  }

}
