import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { ProductDataType, ProductService } from 'src/app/services/product/product.service';
import { SessionService } from 'src/app/services/storage/session.service';
import { WishlistService } from 'src/app/services/wishlist/wishlist.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageReplyComponent } from 'src/app/components/message-reply/message-reply.component';
import { BoookPropertyComponent } from 'src/app/components/boook-property/boook-property.component';
import { NgForm } from '@angular/forms';
import { ImagesGallaryComponent } from 'src/app/components/images-gallary/images-gallary.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  products: ProductDataType[] = [];
  allRecordsCount = 0;
  pageSize = 8;
  pageEvent: any;
  properties: any[] = [];
  featuresList: string[] = ['POOL', 'POOL_FACING', 'BEACH', 'BEACH_FACING', 'GARDEN', 'GARDEN_FACING', 'AC_ROOMS'];

  constructor(
    private api: ApiService,
    public dialog: MatDialog,
    private session: SessionService,
    private alert: AlertService
  ) { }

  ngOnInit(): void {
    this.getProducts(0);
  }
  onPageChange(event: PageEvent) {
    this.getProducts(event.pageIndex);
  }

  getProducts(pageNumber: number) {
    this.api.get(`/api/properties?pageNumber=${pageNumber}&recordsPerPage=${this.pageSize}`)
      .subscribe((res: any) => {
        this.properties = res?.content;
        this.allRecordsCount = res?.totalElements;
      })
  }
  bookProperty(propertyId: number, price: number) {
    const dialogRef = this.dialog.open(BoookPropertyComponent, {
      width: '500px',
      data: { propertyId: propertyId, price: price },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

  showImages(propertyId: number) {
    console.log('propertyId', propertyId);

    const dialogRef = this.dialog.open(ImagesGallaryComponent, {
      width: '700px',
      data: { propertyId: propertyId, },
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }
  onReply(sender: number) {
    const dialogRef = this.dialog.open(MessageReplyComponent, {
      width: '500px',
      data: { senderId: sender, receiverId: this.session.getUsers()?.user?.userId },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

  rateAr(range: number) {
    return Array(range).fill(1);
  }

  formSubmit(formRef: NgForm) {
    if (formRef.form.status === 'INVALID') {
      this.alert.show({
        title: "Required fields",
        message: "Please fill all required fields",
        icon: 'warning'
      });
      return;
    }
    const values = formRef.form.value;
    console.log(values);
    this.api.post('/api/search', {
      searchBy: values?.searchBy,
      checkInDate: "2022-09-04T07:37:04.731Z",
      checkOutDate: "2022-09-04T07:37:04.731Z",
      placeToVisit: values?.place || 'em',
      propertyType: values?.type || 'FLAT',
      feature: values?.feature || 'POOL'
    }).subscribe((res: any) => {
      this.properties = res?.data || [];
    });

  }
}
